require('dotenv').config();
const express = require('express');
const router = require('./routes/index.route');
const app = express();
const port = process.env.PORT;

// SWAGGER
const swaggerUi = require('swagger-ui-express');
const apiDocumentation = require('./api-documentation/swagger-autogen-output.json');
app.use('/docs', swaggerUi.serve, swaggerUi.setup(apiDocumentation));
// ENDSWAGGER

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api/v1', router);

app.listen(port, () => {
    console.log(`server berjalan di port ${port}`);
});


// error handler
app.use(function (err, req, res, next) {
    // render the error page
    res.status(err.status || 500);
    res.json({
        message: "Page not Found!"
    });
});



// Penjelasan
/*
Langkah 1 : 
jalankan sequelize db:create 
untuk membuat database

Langkah 2:
jalankan sequelize db:migrate 
untuk menjalankan migration dan membuat table

langkah 3: 
jalankan sequelize db:seed:all
untuk menjalankan seeder dan membuat data yang akan dimasukkan table

Langkah 4:
jalankan npm start

Langkah 5:
akses swagger dengan mengunjungi url http://localhost:3000/docs/

Langkah 6:
akses Authorization /api/v1/usergame/signup di swagger
untuk SignUp dulu untuk membuat user dan membuat JWT token

Langkah 7:
akses Authorization /api/v1/usergame/login di swagger untuk login menggunakan email dan password
yang telah dibuat di signup tadi untuk memperoleh token dan copy tokennya

Langkah 8:
Setelah itu akses Authorize dan masukkan token yang dicopy tadi
dengan cara memasukkan "Bearer " tambahkan tokennya setelah Bearer spasi

Langkah 9:
Sehingga sekarang route bisa diakses


*/