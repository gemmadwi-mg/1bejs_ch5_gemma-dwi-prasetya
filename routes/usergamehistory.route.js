const express = require('express');
const router = express.Router();
const controller = require('../controllers/index')
const validate = require('../middlewares/validate')
const validation = require('../validations/usergamehistory')


router.get('/', controller.auth.protect, controller.usergamehistory.getAll);
router.post('/', controller.auth.protect, validation.create(), validate, controller.usergamehistory.post);
router.get('/:user_game_history_id', controller.auth.protect, validation.findById(), validate, controller.usergamehistory.getUserGameHistoryById);
router.put('/:user_game_history_id', controller.auth.protect, validation.update(), validate, controller.usergamehistory.put);
router.delete('/:user_game_history_id', controller.auth.protect, validation.destroy(), validate, controller.usergamehistory.delete);

module.exports = router;